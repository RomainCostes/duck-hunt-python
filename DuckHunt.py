import pygame,sys,random
from pygame.locals import*
from sys import exit
from random import randint

WINDOW_WIDTH, WINDOW_HEIGHT = 890, 550
BLACK =       ( 0 ,  0 ,  0 )
WHITE =       (255, 255, 255)
RED =         (255,  0 ,  0 )
DARK_RED =    (200,  0 ,  0 )
GREEN =       ( 0 , 255,  0 )
DARK_GREEN =  ( 0 , 200,  0 )
DARK_YELLOW = (200, 200,  0 )
YELLOW =      (255, 255,  0 )
white =       (255, 255, 255)
DUCK_SIZE = 75
MIN_SPEED, MAX_SPEED = 3, 15
GAME_FONT = 'ComicSansMS.ttf'
pygame.display.set_caption('Duck Hunt by J&R') # sets the window title
pygame.mixer.init(44100, -16, 2, 1024) # Initialize the mixer module for Sound loading and play
pygame.mixer.music.set_volume(1)

pygame.init()

def draw_text(surf, font, msg, pos, topleft=False, topright=False, midtop=False, midbottom=False):
    FONT = pygame.font.Font(GAME_FONT, font['size'])
    textSurf = FONT.render(msg, True, font['col'])
    textRect = textSurf.get_rect()
    if topleft: textRect.topleft = pos
    elif topright: textRect.topright = pos
    elif midtop: textRect.midtop = pos
    elif midbottom: textRect.midbottom = pos
    elif not (topleft and topright): textRect.center = pos
    surf.blit(textSurf, textRect)
def button(surface, msg, pos_x, pos_y, width, height, acolor, icolor, func, mouseClick): #func = pause, quit etc...
    mousePos = pygame.mouse.get_pos()
    if pos_x + width > mousePos[0] > pos_x and pos_y + height > mousePos[1] > pos_y:
        pygame.draw.rect(surface, acolor, (pos_x, pos_y, width, height))
        if mouseClick: func()
    else:
        pygame.draw.rect(surface, icolor, (pos_x, pos_y, width, height))
        pygame.draw.rect(surface, BLACK, (pos_x, pos_y, width, height), 2)
        draw_text(surface, {'size': 18, 'col': BLACK},
        msg, (pos_x + (width / 2), pos_y + (height / 2)))


class Duck:
    def __init__(self, speed, y, direction):
        self.speed = speed
        self.image = pygame.transform.scale(pygame.image.load('greenduck.png'),(DUCK_SIZE, DUCK_SIZE))
        self.direction = direction
        self.rect = self.image.get_rect()
        self.rect.y =  y
        if self.direction == 0:
            self.rect.x = WINDOW_WIDTH
            self.image = pygame.transform.flip(self.image, 1, 0)
        else:
            self.rect.x = -DUCK_SIZE
    def update(self):
        if self.direction == 0:
            self.rect.x -= self.speed
        else:
            self.rect.x += self.speed

# ------------------------------ MANIPULATION ------------------------------#
class Game:
    def __init__(self):
        self.WINDOW = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
        pygame.display.set_caption('Duck Hunt by J&R')
        self.FPS = 40
        self.CLOCK = pygame.time.Clock()
        pygame.mouse.set_cursor(*pygame.cursors.broken_x)
        self.mouseClick = False
        self.clickPos = [-100, -100]
        self.file = open('highscore.txt', 'r')
        self.highscore = int(self.file.read())
        self.file.close()
        self.gameStarted = False
        self.paused = False
    
    def askquit(self):    #menu when ESC or try to close the window
        self.askingaskquit = True
        while self.askingaskquit:
            self.mouseClick = False
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    if event.key == K_RETURN and not self.paused and\
                    self.gameStarted:
                        for x in range(3): # for x in [0, 1, 2]
                            self.WINDOW.fill(WHITE)
                            draw_text(self.WINDOW, {'size': 75,'col': BLACK}, str(-x + 3), (WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2))
                            pygame.display.flip()
                            pygame.time.wait(1000)
                    self.askingaskquit = False
                if event.type == MOUSEBUTTONDOWN:
                    if event.button == 1:
                        self.mouseClick = True
            self.WINDOW.blit(pygame.image.load("background.png"), (0, 0))
            draw_text(self.WINDOW, {'size': 50, 'col': BLACK},
                      'Really quit?', (WINDOW_WIDTH / 2, WINDOW_HEIGHT / 5))
            draw_text(self.WINDOW, {'size': 20, 'col': BLACK},
                      'Press Enter to go back to the game',
                      (WINDOW_WIDTH / 2, WINDOW_HEIGHT / 5 + 100))
            button(self.WINDOW, 'Quit', (WINDOW_WIDTH / 2) - 50,
                   (WINDOW_HEIGHT / 5) * 4 + 30, 100, 50, DARK_RED, RED, self.quit,
                   self.mouseClick)
            pygame.display.flip()
            self.CLOCK.tick(self.FPS)
    def quit(self):
        self.file = open('highscore.txt', 'w')
        self.file.write(str(self.highscore))
        self.file.close()
        pygame.display.quit()
        pygame.quit()
        sys.exit()

    #------------ launch the game ------------#
    def new_game(self):
        self.ducks = []
        self.missedDucks = 0
        self.numDucks = 1
        self.hitDucks = 0;
        self.score = 0
        self.i = 1
        self.run()
    def run(self):
        pygame.mixer.music.load("reload.mp3")
        pygame.mixer.music.play()
        self.playing = True
        while self.playing:
            self.mouseClick = False
            self.clickPos = [-100, -100]
            self.events()
            self.update()
            self.draw()
            self.CLOCK.tick(self.FPS)

    #------------- Event parser -------------#
    def events(self):
        for event in pygame.event.get():
            if event.type == QUIT:
                self.askquit()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    self.askquit()
            if event.type == MOUSEBUTTONDOWN:
                pygame.mixer.music.load("tir.mp3")
                pygame.mixer.music.play()
                if event.button == 1:
                    self.mouseClick = True
                self.clickPos = event.pos
    def update(self):
        if len(self.ducks) < self.numDucks:
            self.ducks.append(Duck(random.randrange(MIN_SPEED, MAX_SPEED),
                                       random.randrange(0, WINDOW_HEIGHT - 99),
                                       random.randrange(0, 2)))
        for duck in self.ducks:
            duck.update()
            if duck.rect.right < 0 or duck.rect.left > WINDOW_WIDTH:
                self.ducks.remove(duck)
                self.missedDucks += 1
            if duck.rect.x + DUCK_SIZE > self.clickPos[0] >\
               duck.rect.x and duck.rect.y + DUCK_SIZE >\
               self.clickPos[1] > duck.rect.y:
                if duck in self.ducks:
                    self.ducks.remove(duck)
                    self.score += (duck.speed * 10)
                    self.hitDucks += 1
                    duck.speed += 1
        if self.missedDucks >= 5: self.playing = False
        if self.score > self.highscore: self.highscore = self.score
        if self.score / 1000 > self.i:
            self.numDucks += 1
            self.i += 1
    def draw(self):
        self.WINDOW.blit(pygame.image.load("background.png"), (0, 0))
        for duck in self.ducks:
            self.WINDOW.blit(duck.image, duck.rect)
        draw_text(self.WINDOW, {'size': 20, 'col': WHITE},
                  'Score: {}'.format(self.score),
                  (10, 0), topleft=True)
        draw_text(self.WINDOW, {'size': 20, 'col': WHITE},
                  'Missed Duck: {}'.format(self.missedDucks),
                  (WINDOW_WIDTH - 10, 0), topright=True)
        draw_text(self.WINDOW, {'size': 20, 'col': WHITE},
                  'Hit Duck: {}'.format(self.hitDucks),
                  (WINDOW_WIDTH / 2, 0), midtop=True)
        draw_text(self.WINDOW, {'size': 20, 'col': WHITE},
                  'High Score: {}'.format(self.highscore),
                  (WINDOW_WIDTH / 2, WINDOW_HEIGHT), midbottom=True)
        button(self.WINDOW, 'Pause', WINDOW_WIDTH - 100, WINDOW_HEIGHT - 50,
               100, 50, DARK_YELLOW, YELLOW, self.pause, self.mouseClick)
        pygame.display.flip()




# ------------------------------ ACCUEIL ------------------------------#
    def title_screen(self):
        pygame.mixer.music.load("music.mp3")
        pygame.mixer.music.play()
        self.gameStarted = False
        while not self.gameStarted:
            self.mouseClick = False
            for event in pygame.event.get():
                if event.type == QUIT:
                    self.askquit()
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                            self.askquit()
                if event.type == MOUSEBUTTONDOWN:
                    if event.button == 1:
                        self.mouseClick = True
            self.WINDOW.blit(pygame.image.load("background.png"), (0, 0))
            draw_text(self.WINDOW, {'size': 50, 'col': BLACK},
                      'Duck Hunt', (WINDOW_WIDTH / 2, WINDOW_HEIGHT / 5))
            draw_text(self.WINDOW, {'size': 20, 'col': BLACK},
                      'High Score: {}'.format(self.highscore),
                      (WINDOW_WIDTH / 2, (WINDOW_HEIGHT / 5) + 100))
            button(self.WINDOW, 'Play', (WINDOW_WIDTH / 2) - 170,
                   (WINDOW_HEIGHT / 5) * 4 + 30, 100, 50, DARK_GREEN, GREEN,
                   self.start, self.mouseClick)
            button(self.WINDOW, 'Info', (WINDOW_WIDTH / 2) - 50,
                   (WINDOW_HEIGHT / 5) * 4 + 30, 100, 50, DARK_YELLOW, YELLOW,
                   self.info, self.mouseClick)
            button(self.WINDOW, 'Quit', (WINDOW_WIDTH / 2) + 70,
                   (WINDOW_HEIGHT / 5) * 4 + 30, 100, 50, DARK_RED, RED,
                   self.askquit, self.mouseClick)
            pygame.display.flip()
            self.CLOCK.tick(self.FPS)
            
    def info(self):
        self.inInfo = True
        while self.inInfo:
            self.mouseClick = False
            for event in pygame.event.get():
                if event.type == QUIT:
                    self.askquit()
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        self.askquit()
                if event.type == MOUSEBUTTONDOWN:
                    if event.button == 1:
                        self.mouseClick = True
            self.WINDOW.blit(pygame.image.load("background.png"), (0, 0))
            draw_text(self.WINDOW, {'size': 50, 'col': BLACK}, 'How to Play',
                      (WINDOW_WIDTH / 2, WINDOW_HEIGHT / 5))
            draw_text(self.WINDOW, {'size': 20, 'col': BLACK},
                      '- Shoot ducks (Click on the ducks)',
                      (WINDOW_WIDTH / 2, WINDOW_HEIGHT / 3))
            draw_text(self.WINDOW, {'size': 20, 'col': BLACK},
                      '- If you miss 5 ducks, the game is over',
                      (WINDOW_WIDTH / 2, (WINDOW_HEIGHT / 3) + 40))
            button(self.WINDOW, 'Got it', (WINDOW_WIDTH / 2) - 50,
                       (WINDOW_HEIGHT / 5) * 4 + 30, 100, 50, DARK_GREEN, GREEN,
                   self.back, self.mouseClick)
            pygame.display.flip()
            self.CLOCK.tick(self.FPS)

#-------------------------------------------------------------------------------#
    def pause(self):
        self.paused = True
        while self.paused:
            self.mouseClick = False
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    if event.key == K_RETURN:
                        for x in range(3):
                            self.WINDOW.fill(WHITE)
                            draw_text(self.WINDOW, {'size': 75,
                                                         'col': BLACK},
                                      str(-x + 3), (WINDOW_WIDTH / 2,
                                                    WINDOW_HEIGHT / 2))
                            pygame.display.flip()
                            pygame.time.wait(1000)
                        self.paused = False
                if event.type == MOUSEBUTTONDOWN:
                    if event.button == 1:
                        self.mouseClick = True
            self.WINDOW.blit(pygame.image.load("background.png"), (0, 0))
            draw_text(self.WINDOW, {'size': 50, 'col': BLACK},
                      'Paused', (WINDOW_WIDTH / 2, WINDOW_HEIGHT / 5))
            draw_text(self.WINDOW, {'size': 20, 'col': BLACK},
                      'Press Enter to go back to the game',
                      (WINDOW_WIDTH / 2, WINDOW_HEIGHT / 5 + 100))
            draw_text(self.WINDOW, {'size': 20, 'col': BLACK},
                      'Score: {}'.format(self.score),
                      (10, 0), topleft=True)
            draw_text(self.WINDOW, {'size': 20, 'col': BLACK},
                      'Missed Ducks: {}'.format(self.missedDucks),
                      (WINDOW_WIDTH - 10, 0), topright=True)
            draw_text(self.WINDOW, {'size': 20, 'col': BLACK},
                      'Hit Ducks: {}'.format(self.hitDucks),
                      (WINDOW_WIDTH / 2, 0), midtop=True)
            draw_text(self.WINDOW, {'size': 20, 'col': BLACK},
                      'High Score: {}'.format(self.highscore),
                      (WINDOW_WIDTH / 2, WINDOW_HEIGHT), midbottom=True)
            button(self.WINDOW, 'Quit', (WINDOW_WIDTH / 2) - 130,
                   (WINDOW_HEIGHT / 5) * 4 + 30, 120, 50, DARK_RED, RED,
                   self.askquit, self.mouseClick)
            button(self.WINDOW, 'End Game', (WINDOW_WIDTH / 2) + 10,
                   (WINDOW_HEIGHT / 5) * 4 + 30, 120, 50, DARK_YELLOW, YELLOW,
                   self.end, self.mouseClick)
            pygame.display.flip()
            self.CLOCK.tick(self.FPS)
    def game_over(self):
        self.gameOver = True
        pygame.mixer.music.load("gameover.mp3")
        pygame.mixer.music.play()
        while self.gameOver:
            self.mouseClick = False
            for event in pygame.event.get():
                if event.type == QUIT:
                    self.askquit()
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        self.askquit()
                if event.type == MOUSEBUTTONDOWN:
                    if event.button == 1:
                        self.mouseClick = True
            self.WINDOW.blit(pygame.image.load("background.png"), (0, 0))
            self.WINDOW.blit(pygame.image.load("dog.gif"), (400, 340))
            draw_text(self.WINDOW, {'size': 50, 'col': BLACK}, 'Game Over',
                      (WINDOW_WIDTH / 2, WINDOW_HEIGHT / 5))
            draw_text(self.WINDOW, {'size': 20, 'col': BLACK},
                      'Score: {}'.format(self.score),
                      (WINDOW_WIDTH / 2, (WINDOW_HEIGHT / 5) + 50))
            draw_text(self.WINDOW, {'size': 20, 'col': BLACK},
                      'High Score: {}'.format(self.highscore),
                      (WINDOW_WIDTH / 2, (WINDOW_HEIGHT / 5) + 100))
            draw_text(self.WINDOW, {'size': 20, 'col': WHITE},
                      'Hit Ducks: {}'.format(self.hitDucks),
                      (WINDOW_WIDTH/ 2, (WINDOW_HEIGHT / 5) + 150))
            button(self.WINDOW, 'Play again', (WINDOW_WIDTH / 2) - 180,
                   (WINDOW_HEIGHT / 5) * 4 + 30, 100, 50, DARK_GREEN, GREEN,
                   self.restart, self.mouseClick)
            button(self.WINDOW, 'Title Screen', (WINDOW_WIDTH / 2) - 60,
                   (WINDOW_HEIGHT / 5) * 4 + 30, 120, 50, DARK_YELLOW, YELLOW,
                   self.to_title_screen, self.mouseClick)
            button(self.WINDOW, 'Quit', (WINDOW_WIDTH / 2) + 80,
                   (WINDOW_HEIGHT / 5) * 4 + 30, 100, 50, DARK_RED, RED,
                   self.askquit, self.mouseClick)
            pygame.display.flip()
            self.CLOCK.tick(self.FPS)
    def start(self):
        self.gameStarted = True
    def back(self): self.inInfo = False
    def restart(self): self.gameOver = False
    def end(self):
        self.playing = False
        self.paused = False
    def to_title_screen(self):
        self.gameOver = False
        self.title_screen()


game = Game()
game.title_screen()
while True:
    game.new_game()
    game.game_over()